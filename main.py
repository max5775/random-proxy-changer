import random

proxy = []
listProxy = {'http': [
    '217.23.69.146:3128',
    '178.566.182.76:3128',
    '176.120.210.13:41258',
    '62.113.113.155:3128',
    '94.228.192.197:8087',
    '95.165.4.178:8080',
    ]}


def get_proxy():
    """Random select proxy from listProxy"""
    for ip in listProxy.values():
        item = random.randrange(1, len(ip))
        proxy = ip[item]
    return proxy
